<?php
/**
* Description: Lionlab text-image field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//text column
$title = get_sub_field('header');
$position = get_sub_field('position');
$text = get_sub_field('text');

//image
$img = get_sub_field('bg_img');


//section settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');

if ($position === 'left') {
	$text_column_class = 'col-md-offset-6';
}


?>

<div class="text-image__wrapper padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<section class="text-image"> 

		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="text-image__content col-md-6 <?php echo esc_attr($position); ?> <?php echo esc_attr($text_column_class); ?>">
					
					<?php if ($title) : ?>
						<h2 class="text-image__title"><?php echo $title; ?></h2>
					<?php endif; ?>

					<?php echo $text; ?>
				</div>
				
				
				<div class="text-image__image col-md-6 <?php echo esc_attr($position); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">

			    </div>

			</div>
		</div>

	</section>
</div>
