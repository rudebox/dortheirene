<footer class="footer bg--blue padding--both" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad">
		<div class="row">

			<?php 
				//contact informations
				$adress = get_field('adress', 'options'); 
  				$phone = get_field('phone', 'options'); 
  				$mail = get_field('mail', 'options'); 

  				//logo
  				$logo = get_field('footer_logo', 'options'); 
			 ?>

			<div class="footer__item col-sm-12 center">
				<a href="/">
					<img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>"><br>
				</a>
				<div class="flex flex--wrap flex--valign footer__links">
					Tlf: <a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a> <a href="mailto:<?php echo esc_attr($mail); ?>"><?php echo esc_html($mail); ?></a> <?php echo esc_html($adress); ?>
				</div>
			</div>

		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
